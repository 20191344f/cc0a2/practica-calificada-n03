package pe.uni.dvasquezl.practicacalificadan03;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.widget.Button;
import android.widget.RadioButton;

import com.google.android.material.snackbar.Snackbar;

public class MainActivity extends AppCompatActivity {

    RadioButton radioButton1, radioButton2, radioButton3;
    Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        radioButton1 = findViewById(R.id.radio_button_1);
        radioButton2 = findViewById(R.id.radio_button_2);
        radioButton3 = findViewById(R.id.radio_button_3);

        button = findViewById(R.id.button_start);

        button.setOnClickListener(view -> {
            if (!radioButton1.isChecked() && !radioButton2.isChecked() && !radioButton3.isChecked()) {
                Snackbar.make(view, R.string.snack_bar_msg, Snackbar.LENGTH_LONG).show();
                return;
            }

            Intent intent = new Intent(MainActivity.this, BasicOptionActivity.class);
            if (radioButton1.isChecked()) {
                intent.putExtra("BASIC", true);
            }

            Resources res = getResources();

            if (radioButton2.isChecked()) {
                intent.putExtra("SCIENTIFIC", true);

                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setTitle(R.string.dialog_title);
                builder.setCancelable(false);
                builder.setMessage(String.format(res.getString(R.string.calculator_message), res.getString(R.string.radio_button_2_text)));
                builder.setPositiveButton("Si", (dialogInterface, i) -> {
                });
                builder.setNegativeButton("No", (dialogInterface, i) -> {
                    moveTaskToBack(true);
                    android.os.Process.killProcess(android.os.Process.myPid());
                });
                builder.create().show();
                return;
            }

            if (radioButton3.isChecked()) {
                intent.putExtra("DEVELOPER", true);

                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setTitle(R.string.dialog_title);
                builder.setCancelable(false);
                builder.setMessage(String.format(res.getString(R.string.calculator_message), res.getString(R.string.radio_button_3_text)));
                builder.setPositiveButton("Si", (dialogInterface, i) -> {
                });
                builder.setNegativeButton("No", (dialogInterface, i) -> {
                    moveTaskToBack(true);
                    android.os.Process.killProcess(android.os.Process.myPid());
                });
                builder.create().show();
                return;
            }

            startActivity(intent);
        });
    }
}