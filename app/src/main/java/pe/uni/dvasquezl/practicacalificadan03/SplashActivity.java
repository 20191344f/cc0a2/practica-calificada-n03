package pe.uni.dvasquezl.practicacalificadan03;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

@SuppressLint("CustomSplashScreen")
public class SplashActivity extends AppCompatActivity {

    ImageView imageViewSplash;
    TextView textViewTitle;
    Animation animationImage, animationText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        imageViewSplash = findViewById(R.id.image_view_splash);
        textViewTitle = findViewById(R.id.text_view_splash);

        animationImage = AnimationUtils.loadAnimation(this, R.anim.image_animation_splash);
        animationText = AnimationUtils.loadAnimation(this, R.anim.text_animation_splash);

        imageViewSplash.setAnimation(animationImage);
        textViewTitle.setAnimation(animationText);

        new CountDownTimer(6000, 1000) {

            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
                Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        }.start();
    }
}